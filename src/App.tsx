import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { Menu } from './components/Menu/Menu';
import { PostsPage } from './pages/Posts/PostsPage';
import { PostPage } from './pages/Post/Post';
import './App.css';


function App() {
  return (
    <div className="container app">
      <Router>
        <Menu />
        <main>
          <Switch>
            <Route path="/posts" component={PostsPage} />
            <Route path="/post/:slug" component={PostPage} />
            <Route path="/users">
              <div>Page is not implemented</div>
            </Route>
            <Route path="/">
              <h1>Welcome to User App</h1>
            </Route>
          </Switch>
        </main>
      </Router>
    </div>
  );
}

export default App;
