import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { DbProvider } from './services/db.provider';
import { database, up } from 'migrate-mongo';

async function bootstrap() {
  // App
  const app = await NestFactory.create(AppModule);

  // DB
  const dbProvider = await app.resolve(DbProvider);
  await dbProvider.isReady;

  // Migrations
  const { db } = await database.connect();
  await up(db);

  // SWAGGER
  const config = new DocumentBuilder()
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);

  app.enableCors();
  await app.listen(3001);
}
bootstrap();
