import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { ApiBody, ApiCreatedResponse, ApiOkResponse, ApiParam, ApiTags } from '@nestjs/swagger';
import { UserService } from '../services/user.service';
import { UserModel } from '../models/UserModel';

@ApiTags('User')
@Controller('user')
export class UserController {

  constructor(private userService: UserService) {}

  @ApiOkResponse({ description: 'List of users' })
  @Get()
  public getAll(): Promise<Array<Partial<UserModel>>> {
    return this.userService.getUserList();
  }

  @ApiParam({ name: 'id', type: String, required: true })
  @ApiOkResponse({ description: 'User model' })
  @Get(':id')
  public getUser(@Param('id') id: string): any {
    return this.userService.getUser(id);
  }

  @ApiCreatedResponse({ description: 'User created' })
  @Post()
  public createUser(@Body() user: UserModel): Promise<string> {
    return this.userService.createUser(user);
  }

  @ApiBody({ type: UserModel })
  @ApiParam({ name: 'id', type: String, required: true })
  @Put(':id')
  public updateUser(@Body() user: UserModel, @Param('id') id: string): Promise<void> {
    return this.userService.updateUser(id, user);
  }
}
