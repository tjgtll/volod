import { ObjectId } from 'mongodb';

export class UserModel {

  _id?: string | ObjectId;

  firstName: string;

  lastName: string;

  email: string;

  bio: string;

  phone: string;

  isVerified: boolean;

  birthday: string;

}
